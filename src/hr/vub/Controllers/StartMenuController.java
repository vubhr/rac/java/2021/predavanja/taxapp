package hr.vub.Controllers;

import hr.vub.Models.Database.PensionerDatabase;
import hr.vub.Models.Database.StudentDatabase;
import hr.vub.Models.Database.WorkerDatabase;
import hr.vub.Models.Pensioner;
import hr.vub.Models.Person;
import hr.vub.Models.Student;
import hr.vub.Models.Worker;
import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.TableColumn;
import javafx.scene.control.TableView;
import javafx.scene.control.TextField;
import javafx.scene.control.cell.PropertyValueFactory;

import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;

public class StartMenuController {


    @FXML
    private TableView<Person> table;

    @FXML
    private TableColumn<Person, UUID> id;

    @FXML
    private TableColumn<Person, String> firstName;

    @FXML
    private TableColumn<Person, String> lastName;

    @FXML
    private TableColumn<Person, Double> accountBalance;

    @FXML
    private TextField uuidTextField;

    @FXML
    private TextField firstNameTextField;

    @FXML
    private TextField lastNameTextField;

    @FXML
    void Add(ActionEvent event) {
        PensionerDatabase pdb = new PensionerDatabase();
        String name = firstNameTextField.getText();
        String lastName = lastNameTextField.getText();
        Random rand = new Random();
        pdb.add(new Pensioner(UUID.randomUUID(), name, lastName, rand.nextFloat() * 100000,
                rand.nextDouble() * 10000, rand.nextInt(60)));
    }

    @FXML
    void Delete(ActionEvent event) {
        PensionerDatabase pdb = new PensionerDatabase();
        String inputUUID = uuidTextField.getText();
        Pensioner p = null;
        if(inputUUID.isEmpty()){
            return;
        } else {
            p = pdb.get(UUID.fromString(inputUUID));
        }
        if(p == null){
            return;
        } else {
            uuidTextField.setDisable(false);
            uuidTextField.clear();
            firstNameTextField.clear();
            lastNameTextField.clear();
            pdb.remove(p);
        }

    }


    @FXML
    void GetByUUID(ActionEvent event) {
        PensionerDatabase pdb = new PensionerDatabase();
        String inputUUID = uuidTextField.getText();
        Pensioner p = null;
        if(inputUUID.isEmpty()){
            return;
        } else {
           p = pdb.get(UUID.fromString(inputUUID));
        }
        if(p == null){
            return;
        } else {
            firstNameTextField.setText(p.getFirstName());
            lastNameTextField.setText(p.getLastName());
            uuidTextField.setDisable(true);
        }

    }

    @FXML
    void Update(ActionEvent event) {
        PensionerDatabase pdb = new PensionerDatabase();
        String inputUUID = uuidTextField.getText();
        Pensioner p = null;
        if(inputUUID.isEmpty()){
            return;
        } else {
            p = pdb.get(UUID.fromString(inputUUID));
        }
        if(p == null){
            return;
        } else {
            p.setFirstName(firstNameTextField.getText());
            p.setLastName(lastNameTextField.getText());
            pdb.update(p);
        }

    }


    @FXML
    void getAllDatabaseData(ActionEvent event) {

        id.setCellValueFactory(new PropertyValueFactory<>("uuid"));
        firstName.setCellValueFactory(new PropertyValueFactory<>("firstName"));
        lastName.setCellValueFactory(new PropertyValueFactory<>("lastName"));
        accountBalance.setCellValueFactory(new PropertyValueFactory<>("accountBalance"));

        table.getItems().clear();

        PensionerDatabase pdb = new PensionerDatabase();
        pdb.getAllPensioners().stream().forEach(pensioner -> System.out.println(pensioner.toString()));
        StudentDatabase sdb = new StudentDatabase();
        sdb.getAllStudents().stream().forEach(student -> System.out.println(student.toString()));
        WorkerDatabase wrd = new WorkerDatabase();
        wrd.getAllWorkers().stream().forEach(worker -> System.out.println(worker.toString()));


        List<Pensioner> pensioners = pdb.getAllPensioners();
        List<Student> students = sdb.getAllStudents();
        List<Worker> workers = wrd.getAllWorkers();
        List<Person> persons = new ArrayList<Person>();
        persons.addAll(pensioners);
        persons.addAll(students);
        persons.addAll(workers);

        for (Person p: persons) {
            table.getItems().add(p);
        }
    }

    @FXML
    void logoutAction(ActionEvent event) {
        ScreenController screenController = new ScreenController();
        screenController.switchToSceneLogin(event);
    }



}


