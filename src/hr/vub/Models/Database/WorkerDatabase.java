package hr.vub.Models.Database;

import hr.vub.Models.Worker;
import hr.vub.Models.WorkerRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class WorkerDatabase implements WorkerRepository {

    List<Worker> database = new ArrayList<>();

    public WorkerDatabase(){
        database.add(new Worker(UUID.randomUUID(), "Radnik", "Radnić",40000,
                2, 7502, false ));
        database.add(new Worker(UUID.randomUUID(), "Perica", "Perić",50000,
                3, 10000, false ));
    }

    @Override
    public Worker get(UUID id) {
        for (Worker w: database) {
            if(w.getUuid().compareTo(id) == 0){
                return w;
            }
        }
        return null;
    }

    @Override
    public void add(Worker w) {
        database.add(w);
    }

    @Override
    public boolean update(Worker w) {
        for (Worker worker: database) {
            if(worker.getUuid().compareTo(w.getUuid()) == 0){
                worker.setFirstName(w.getFirstName());
                worker.setLastName(w.getLastName());
                worker.setAccountBalance(w.getAccountBalance());
                worker.setChildrenNumber(w.getChildrenNumber());
                worker.setSalaryInput(w.getSalaryInput());
                worker.setSocialCase(w.isSocialCase());
                return true;
            }
        }
        database.add(w);
        return false;
    }

    @Override
    public void remove(Worker w) {
        database.remove(w);
    }

    @Override
    public List<Worker> getAllWorkers() {
        return database;
    }
}
