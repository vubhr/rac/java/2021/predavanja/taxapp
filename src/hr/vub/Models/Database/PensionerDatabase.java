package hr.vub.Models.Database;

import hr.vub.Models.Pensioner;
import hr.vub.Models.PensionerRepository;
import hr.vub.Utilities.AppConstants;

import java.io.*;
import java.util.ArrayList;
import java.util.List;
import java.util.Random;
import java.util.UUID;
import java.util.logging.Logger;



public class PensionerDatabase implements PensionerRepository {

    static Logger log = Logger.getLogger(AppConstants.LOGGER_STRING);

    private List<Pensioner> database = new ArrayList<Pensioner>();
    private String databaseFilePath = "PensionerDatabase.ser";

    public PensionerDatabase(){
        boolean testForDatabase = checkForDatabase();
        if(testForDatabase == true){
            readDataFromDatabase();
        } else {
            generateDataForDatabase();
        }
    }

    private boolean checkForDatabase(){
        File databaseFile = new File(databaseFilePath);
        if(databaseFile.exists() && databaseFile.isFile()){
            return true;
        }
        return false;
    }

    private void generateDataForDatabase(){
        File databaseFile = new File(databaseFilePath);
        Random random = new Random();
        for(int i = 0; i < 30 ; i++){
            database.add(new Pensioner(UUID.randomUUID(), "Ime penzioner " + Integer.toString(i),
                    "Prezime penzioner" + Integer.toString(i), random.nextFloat() * 100000,
                    random.nextDouble() * 10000, random.nextInt(60)));
        }

        try (FileOutputStream fo = new FileOutputStream(databaseFile,false);
             ObjectOutputStream ou = new ObjectOutputStream(fo)) {
            ou.writeObject(database);
            log.info("Pensioner database successfully created!");
        }
        catch (Exception e){
            e.printStackTrace();
            log.warning("Problem with pensioner database creation!" + e.getMessage());
        }
    }

    private void readDataFromDatabase(){
        File databaseFile = new File(databaseFilePath);
        try(FileInputStream fi = new FileInputStream(databaseFile);
            ObjectInputStream oi = new ObjectInputStream(fi);){
            database = (ArrayList<Pensioner>) oi.readObject();
            log.info("Pensioner database successfully loaded!");

        }catch (Exception e){
            e.printStackTrace();
            log.warning("There is problem with loading pensioner database!" + e. getMessage());
        }
    }

    private void updateDataInDatabase(){
        File databaseFile = new File(databaseFilePath);
        try (FileOutputStream fo = new FileOutputStream(databaseFile,false);
             ObjectOutputStream ou = new ObjectOutputStream(fo)) {
            ou.writeObject(database);
            log.info("Pensioner database successfully updated!");
        }
        catch (Exception e){
            e.printStackTrace();
            log.warning("Problem with pensioner database update!" + e.getMessage());
        }
    }


    @Override
    public Pensioner get(UUID id) {
        log.info("Method call!" + id.toString());
        for (Pensioner p: database) {
            if(p.getUuid().compareTo(id) == 0){
                return p;
            }
        }
        return null;
    }

    @Override
    public void add(Pensioner pensioner) {
        log.info("Method call!" + pensioner.toString());
        database.add(pensioner);
        updateDataInDatabase();
    }

    @Override
    public boolean update(Pensioner pensioner) {
        log.info("Method call!" + pensioner.toString());
        for (Pensioner p: database) {
            if(p.getUuid() == pensioner.getUuid()){
                p.setFirstName(pensioner.getFirstName());
                p.setLastName(pensioner.getLastName());
                p.setAccountBalance(pensioner.getAccountBalance());
                p.setWorkingAge(pensioner.getWorkingAge());
                p.setPensionInput(pensioner.getPensionInput());
                updateDataInDatabase();
                return true;

            }
        }
     return false;
    }

    @Override
    public void remove(Pensioner pensioner) {
        log.info("Method call!" + pensioner.toString());
        database.remove(pensioner);
        updateDataInDatabase();
    }

    @Override
    public List<Pensioner> getAllPensioners() {
        log.info("Method call!");
        return database;
    }

}
