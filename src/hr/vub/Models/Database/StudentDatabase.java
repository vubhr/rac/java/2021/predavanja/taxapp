package hr.vub.Models.Database;

import hr.vub.Models.Student;
import hr.vub.Models.StudentRepository;

import java.util.ArrayList;
import java.util.List;
import java.util.UUID;

public class StudentDatabase implements StudentRepository {

    List<Student> database = new ArrayList<Student>();
    public StudentDatabase(){
        database.add(new Student(UUID.randomUUID(), "Studento", "Studentic", 30000, 40));
        database.add(new Student(UUID.randomUUID(), "Marko", "Markic", 20000, 35));
        database.add(new Student(UUID.randomUUID(), "Perica", "Peric", 35000, 30));
    }
    @Override
    public Student get(UUID id) {
        for (Student s: database) {
            if(s.getUuid().compareTo(id) == 0){
                return s;
            }
        }
        return null;
    }
    @Override
    public void add(Student student) {
        database.add(student);
    }
    @Override
    public boolean update(Student student) {
        for (Student s: database) {
            if(s.getUuid() == student.getUuid()){
                s.setFirstName(student.getFirstName());
                s.setLastName(student.getLastName());
                s.setAccountBalance(student.getAccountBalance());
                s.setHourlyRate(student.getHourlyRate());
                return true;
            }
        }
        database.add(student);
        return false;
    }
    @Override
    public void remove(Student student) {
        database.remove(student);
    }
    @Override
    public List<Student> getAllStudents() {
        return database;
    }
}
