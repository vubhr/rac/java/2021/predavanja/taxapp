package hr.vub.Models;

import java.util.List;
import java.util.UUID;

public interface WorkerRepository {
    Worker get(UUID id);
    void add(Worker w);
    boolean update(Worker w);
    void remove(Worker w);
    List<Worker> getAllWorkers();
}
