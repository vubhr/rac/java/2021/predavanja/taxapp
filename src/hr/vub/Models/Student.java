package hr.vub.Models;

import hr.vub.Utilities.AppConstants;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Student extends Person implements Serializable {

    protected double hourlyRate;

    private double hourlyRateThreshold = 25;

    public Student(UUID uuid, String firstName, String lastName, double accountBalance, double hourlyRate) {
        super(uuid, firstName, lastName, accountBalance);
        this.hourlyRate = hourlyRate;
    }
    //Copy constructor
    public Student(Student original) {
        super(UUID.randomUUID(), original.firstName, original.lastName, original.accountBalance);
        this.hourlyRate = original.hourlyRate;
    }

    public double getHourlyRate() {
        return hourlyRate;
    }

    public void setHourlyRate(double hourlyRate) {
        this.hourlyRate = hourlyRate;
    }


    @Override
    public String toString() {
        return "Student{" +
                "hourlyRate=" + hourlyRate +
                ", uuid=" + uuid +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Student student = (Student) o;
        return Double.compare(student.getHourlyRate(), getHourlyRate()) == 0;
    }

    @Override
    public int hashCode() {
        return Objects.hash(getHourlyRate());
    }

    @Override
    public boolean applyTax() {
        double calculatedTax = 0;
        if(hourlyRate > hourlyRateThreshold){
            calculatedTax = accountBalance * AppConstants.TAX_STUDENT;
            accountBalance = accountBalance - calculatedTax;
            return true;
        } else {
            return false;
        }
    }

    @Override
    public double getCalculatedTax() {
        double calculatedTax;
        if(hourlyRate > hourlyRateThreshold){
            calculatedTax = accountBalance * AppConstants.TAX_STUDENT;
            return  calculatedTax;
        } else {
            return calculatedTax = 0;
        }

    }


}
