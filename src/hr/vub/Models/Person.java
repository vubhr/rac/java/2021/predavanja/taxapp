package hr.vub.Models;

import java.io.Serializable;
import java.util.UUID;

public abstract class Person implements Serializable {

    protected UUID uuid;
    protected String firstName;
    protected String lastName;
    protected double accountBalance;

    public Person(){};

    public Person(UUID uuid, String firstName, String lastName, double accountBalance) {
        this.uuid = uuid;
        this.firstName = firstName;
        this.lastName = lastName;
        this.accountBalance = accountBalance;
    }

    public abstract boolean applyTax();
    public abstract double getCalculatedTax();

    public UUID getUuid() {
        return uuid;
    }

    public void setUuid(UUID uuid) {
        this.uuid = uuid;
    }

    public String getFirstName() {
        return firstName;
    }

    public void setFirstName(String firstName) {
        this.firstName = firstName;
    }

    public String getLastName() {
        return lastName;
    }

    public void setLastName(String lastName) {
        this.lastName = lastName;
    }

    public double getAccountBalance() { return accountBalance; }

    public void setAccountBalance(double accountBalance) { this.accountBalance = accountBalance; }
}
