package hr.vub.Models;

import hr.vub.Utilities.AppConstants;

import java.io.Serializable;
import java.util.Objects;
import java.util.UUID;

public class Worker extends Person implements Serializable {

    protected int childrenNumber;
    protected int salaryInput;
    protected boolean socialCase;

    private final double salaryInputClassHigh = 0.3;
    private final double salaryInputClassMid = 0.2;
    private final double salaryInputClassLow = 0.1;

    public boolean isSocialCase() {
        return socialCase;
    }

    public void setSocialCase(boolean socialCase) {
        this.socialCase = socialCase;
    }

    private final int salaryInputHighThreshold = 10000;
    private final int salaryInputMidThreshold = 7500;

     public Worker(UUID uuid, String firstName, String lastName, double accountBalance, int childrenNumber, int salaryInput,  boolean socialCase) {
        super(uuid, firstName, lastName, accountBalance);
        this.childrenNumber = childrenNumber;
        this.salaryInput = salaryInput;
        this.socialCase = socialCase;
    }

    public Worker(Worker original){
        super(UUID.randomUUID(), original.firstName, original.lastName, original.accountBalance);
        this.childrenNumber = original.childrenNumber;
        this.salaryInput = original.salaryInput;
        this.socialCase = original.socialCase;
    }

    public int getChildrenNumber() {
        return childrenNumber;
    }

    public void setChildrenNumber(int childrenNumber) {
        this.childrenNumber = childrenNumber;
    }

    public int getSalaryInput() {
        return salaryInput;
    }

    public void setSalaryInput(int salaryInput) {
        this.salaryInput = salaryInput;
    }

    @Override
    public String toString() {
        return "Worker{" +
                "childrenNumber=" + childrenNumber +
                ", salaryInput=" + salaryInput +
                ", socialCase=" + socialCase +
                ", salaryInputClassHigh=" + salaryInputClassHigh +
                ", salaryInputClassMid=" + salaryInputClassMid +
                ", salaryInputClassLow=" + salaryInputClassLow +
                ", salaryInputHighThreshold=" + salaryInputHighThreshold +
                ", salaryInputMidThreshold=" + salaryInputMidThreshold +
                ", uuid=" + uuid +
                ", firstName='" + firstName + '\'' +
                ", lastName='" + lastName + '\'' +
                ", accountBalance=" + accountBalance +
                '}';
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Worker worker = (Worker) o;
        return getChildrenNumber() == worker.getChildrenNumber() &&
                getSalaryInput() == worker.getSalaryInput() &&
                socialCase == worker.socialCase &&
                Double.compare(worker.salaryInputClassHigh, salaryInputClassHigh) == 0 &&
                Double.compare(worker.salaryInputClassMid, salaryInputClassMid) == 0 &&
                Double.compare(worker.salaryInputClassLow, salaryInputClassLow) == 0 &&
                salaryInputHighThreshold == worker.salaryInputHighThreshold &&
                salaryInputMidThreshold == worker.salaryInputMidThreshold;
    }



    @Override
    public int hashCode() {
        return Objects.hash(getChildrenNumber(), getSalaryInput(), socialCase, salaryInputClassHigh, salaryInputClassMid, salaryInputClassLow, salaryInputHighThreshold, salaryInputMidThreshold);
    }

    @Override
    public boolean applyTax() {
        double calculatedTax;
        if(socialCase == true){
            return false;
        }
        else if(salaryInput > salaryInputHighThreshold) {
            calculatedTax = accountBalance * salaryInputHighThreshold * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            accountBalance = accountBalance - calculatedTax;
            return true;
        } else if(salaryInput > salaryInputMidThreshold && salaryInput < salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputHighThreshold * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            accountBalance = accountBalance - calculatedTax;
            return true;
        } else {
            calculatedTax = accountBalance * salaryInputHighThreshold * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            accountBalance = accountBalance - calculatedTax;
            return true;
        }
    }

    @Override
     public double getCalculatedTax() {
        double calculatedTax;
        if(salaryInput > salaryInputHighThreshold) {
            calculatedTax = accountBalance * salaryInputClassHigh * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            return calculatedTax;
        } else if(salaryInput > salaryInputMidThreshold && salaryInput < salaryInputHighThreshold){
            calculatedTax = accountBalance * salaryInputClassMid * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            return calculatedTax;
        } else {
            calculatedTax = accountBalance * salaryInputClassLow * AppConstants.TAX_WORKER;
            calculatedTax = calculatedTax - (calculatedTax * (childrenNumber/10d));
            return calculatedTax;
        }
    }
}
