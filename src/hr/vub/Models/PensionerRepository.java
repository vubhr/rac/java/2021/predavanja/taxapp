package hr.vub.Models;

import java.util.List;
import java.util.UUID;

public interface PensionerRepository {
    Pensioner get(UUID id);
    void add(Pensioner pensioner);
    boolean update(Pensioner pensioner);
    void remove(Pensioner pensioner);
    List<Pensioner> getAllPensioners();

}
