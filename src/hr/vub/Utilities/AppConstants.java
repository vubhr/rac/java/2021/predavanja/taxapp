package hr.vub.Utilities;

public class AppConstants {
    public static final double TAX_STUDENT = 0.1;
    public static final double TAX_WORKER = 0.2;
    public static final double TAX_PENSIONER = 0.05;
    public static final String LOGGER_STRING = "hr.vub.TaxApplication";
}
